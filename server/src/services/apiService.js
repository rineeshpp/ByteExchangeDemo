import circularJSON from 'circular-json'
import axios from 'axios'
import qs from 'qs'

function request (options) {
    const requestOptions = generateRequestOptions(options)
    console.info(
        circularJSON.stringify({ Microservice_Request: requestOptions })
    )
    return axios(requestOptions)
}

function generateRequestOptions ({ reqLocals, headers = {}, ...others }) {
    const updatedOptions = { ...others }

    updatedOptions.headers = {
        ...reqLocals.forwardedHeaders,
        ...headers
    }
    updatedOptions.paramsSerializer = param => qs.stringify(param, { indices: false })
    return updatedOptions
}

function logResponse (response) {
    const {
        status, config, headers, data
    } = response
    console.info(
        circularJSON.stringify({
            Microservice_Response: {
                status,
                config,
                headers,
                data
            }
        })
    )
    // if (response.status === 200) {
    // } else {}
}

module.exports = {
    get: options => new Promise((resolve, reject) => {
        request(options)
            .then((response) => {
                logResponse(response)
                resolve(response.data)
            })
            .catch((error) => {
                const { response = {} } = error
                logResponse(error)
                response.status = response.status || 500
                reject(response)
            })
    }),
    post: options => new Promise((resolve, reject) => {
        const requestOptions = {
            method: 'post',
            ...options
        }

        request(requestOptions)
            .then((response) => {
                logResponse(response)
                resolve(response.data)
            })
            .catch((error) => {
                const { response = {} } = error
                logResponse(error)
                response.status = response.status || 500
                reject(response)
            })
    }),
    put: options => new Promise((resolve, reject) => {
        const requestOptions = {
            method: 'put',
            ...options
        }

        request(requestOptions)
            .then((response) => {
                logResponse(response)
                resolve(response.data)
            })
            .catch((error) => {
                const { response = {} } = error
                logResponse(error)
                response.status = response.status || 500
                reject(response)
            })
    }),
    getAll: (...requests) => new Promise((resolve, reject) => {
        axios
            .all(requests.map(reqParams => request(reqParams)))
            .then(
                axios.spread((...list) => {
                    list.forEach(response => logResponse(response))
                    resolve(list)
                })
            )
            .catch((error) => {
                const { response = {} } = error
                logResponse(error)
                response.status = response.status || 500
                reject(response)
            })
    }),
    keyMap: (inputObject, keyMapping, outputObject = {}) => {
        const mappedObject = { ...outputObject }

        Object.entries(keyMapping).forEach(([ outputKey, inputkey ]) => {
            /**
       * Use Object prototype's hasOwnProperty method.
       * This will work even if the inputObject doesn't have any prototype methods.
       */
            if (Object.prototype.hasOwnProperty.call(inputObject, inputkey)) {
                mappedObject[outputKey] = inputObject[inputkey]
            } else {
                const mappingFunction = keyMapping[outputKey]
                if (typeof mappingFunction === 'function') {
                    mappedObject[outputKey] = mappingFunction(inputObject)
                }
            }
        })

        return mappedObject
    }

}
