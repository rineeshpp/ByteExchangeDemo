const errorHandler = (err, req, res, next) => {
    if (err instanceof Error) {
        let {
            response: {
                data: { meta: { displayMessage, message: errMessage } = {} } = {}
            } = {}
        } = err
        let message =
      displayMessage ||
      errMessage ||
      err?.displayMessage ||
      err?.message ||
      'Something went wrong'
        let status = err?.status || err?.response?.status || 400
        let errResObj = {
            meta: {
                displayMessage: message,
                code: status
            }
        }
        res.status(status).json(errResObj)
    }
}
export default errorHandler
