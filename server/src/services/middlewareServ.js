
import { ENV } from './../config/environment'
import morgan from 'morgan'
import uuid from 'uuid/v4'
import stringify from 'fast-json-stable-stringify'
import { customHeader, customHeaderKeys } from '../constants/custom-header'
import { SYSTEM_TYPE, TOKEN_CONFIG } from '../constants'

morgan.token('req-header', req => req.headers || undefined)
morgan.token('res-header', (_, res) => res._headers || undefined)
morgan.token('request-id', (_, res) => res.locals.byteScope.requestId || undefined)
morgan.token('response-id', (_, res) => res.locals.byteScope.responseId || undefined)
morgan.token('response-body', (req, res) => res.__morgan_body_response || undefined)
morgan.token('request-body', (req, res) => req.body || undefined)

const formatterLogger = (tokens, req, res) => {
    return stringify({
        env: ENV,
        method: tokens.method(req, res),
        url: tokens.url(req, res),
        status: tokens.status(req, res),
        length: tokens.res(req, res, 'content-length'),
        duration: tokens['response-time'](req, res) + ' ms',
        remoteAddr: tokens['remote-addr'](req, res),
        remoteUser: tokens['remote-user'](req, res),
        requestHeader: tokens['req-header'](req, res),
        responseHeader: tokens['res-header'](req, res),
        requestId: tokens['request-id'](req, res),
        responseId: tokens['response-id'](req, res),
        responseBody: tokens['response-body'](req, res),
        requestBody: tokens['request-body'](req, res),
        'date': tokens.date('iso')
    })
}
const loggerMiddleware = morgan((tokens, req, res) => formatterLogger(tokens, req, res), {
    skip: function (req, res) { return res.statusCode < 400 }
})

const middlewareHeader = (req, res, next) => {
    if (ENV !== 'production' && ENV !== 'preprod' && ENV !== 'staging') {
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
    }
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET')
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Content-Type, Authorization, Access-Control-Allow-Headers, X-Requested-With, x-client-utc-offset'
    )
    res.setHeader('Access-Control-Allow-Credentials', true)
    next()
}
const getAllRequestHeaders = (requestHeaders) => {
    const mappedHeaders = customHeaderKeys.reduce((obj, key) => {
        if (requestHeaders[key]) {
            obj[key] = requestHeaders[key]
        }
        return obj
    }, {})
    return mappedHeaders
}
const localsMiddleware = (req, res, next) => {
    res.locals.byteScope = res.locals.byteScope || {}
    res.locals.byteScope.requestId = uuid()
    next()
}
const customForwardHeaders = (req, res, next) => {
    res.locals.byteScope.forwardedHeaders = getAllRequestHeaders(req.headers)
    res.locals.byteScope.forwardedHeaders[customHeader.REQUEST_ID] = res.locals.byteScope.requestId
    next()
}
const authMiddleWare = (req, res, next) => {
    res.locals.byteScope.forwardedHeaders[customHeader.SYS_TYPE] = SYSTEM_TYPE
    next()
}

const panelMiddleware = (req, res, next) => {
    const { cookies, headers } = req
    res.locals.byteScope = {
        token: cookies[TOKEN_CONFIG.APP_TOKEN] || headers[TOKEN_CONFIG.APP_TOKEN],
        'sys-type': SYSTEM_TYPE
    }
    next()
}
const tokenHandler = (req, res, next) => {
    const appToken = req.cookies[TOKEN_CONFIG.APP_TOKEN] || req.headers[TOKEN_CONFIG.APP_TOKEN]
    if (!appToken) {
        return res.status(400).send({ error: 'Token is missing' })
    }
    next()
}
export {
    middlewareHeader,
    customForwardHeaders,
    loggerMiddleware,
    localsMiddleware,
    panelMiddleware,
    authMiddleWare,
    tokenHandler
}
