import { FORMAT_TYPES } from '.'
export const commonTableMapper = {

    tableData: {
        rowHeader: [
            { id: 'userId', title: 'User ID', sortable: true },
            { id: 'orderId', title: 'Order ID', sortable: true },
            { id: 'pran', title: 'PRAN Number', sortable: true },
            { id: 'pan', title: 'PAN Number', sortable: true },
            { id: 'schemePreference', title: 'Scheme Preference', isNestedKey: true, nestedKey: 'tier2SchemePreference.tier2SchemePreference', sortable: true },
            { id: 'transactionStatus', title: 'Transaction Status', isNestedKey: true, nestedKey: 'tier1TransactionStatus.tier2TransactionStatus', sortable: true },
            { id: 'paymentMethod', title: 'Payment Method', sortable: true },
            { id: 'tier1MasterTransactionId', title: 'Transaction ID 1', sortable: true },
            // { id: 'tier1SchemePreference', title: 'Txn 1 Tier' ,sortable:true},
            { id: 'tier1Amount', title: 'Txn 1 Amount', isFormat: true, formatType: FORMAT_TYPES.INR, sortable: true },
            { id: 'tier1FundName', title: 'Txn 1 Fund Name', sortable: true },
            { id: 'collectionPaymentStatus', title: 'Payment txn 1 status', sortable: true },
            { id: 'tier2MasterTransactionId', title: 'Transaction ID 2', sortable: true },
            // { id: 'tier2SchemePreference', title: 'Txn 2 Tier' ,sortable:true},
            { id: 'tier2Amount', title: 'Txn 2 Amount', isFormat: true, formatType: FORMAT_TYPES.INR, sortable: true },
            { id: 'tier2FundName', title: 'Txn 2 Fund Name', sortable: true },
            { id: 'revenuePaymentStatus', title: 'Payment Txn 2 Status', sortable: true },
            { id: 'transactionDate', title: 'Transaction created date', isFormat: true, formatType: FORMAT_TYPES.DATE, sortable: true },
            { id: 'invoiceNumber', title: 'Invoice number', sortable: true },
            { id: 'revenueAmount', title: 'Revenue', isFormat: true, formatType: FORMAT_TYPES.INR, sortable: true },
            { id: 'gst', title: 'GST', isFormat: true, formatType: FORMAT_TYPES.INR, sortable: true }
        ]
    }
}
