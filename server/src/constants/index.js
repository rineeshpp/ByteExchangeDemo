export const TOKEN_CONFIG = {
    INITIAL_TOKEN: 'initial_t',
    USER_TOKEN: 'aut',
    APP_TOKEN: 'app_t'
}
export const SYSTEM_TYPE = 'BYTESCOPE'
export const dateQuery = {
    fromDate: (date) => date.fromDate,
    toDate: (date) => date.toDate
}
export const reportQuery = {
    ...dateQuery
}

export const FORMAT_TYPES = {
    DATE: 'DATE',
    NUMBER: 'NUMBER',
    INR: 'INR'
}
export const paginatonQuery = {
    pageNumber: (query) => query.pageNumber || 0,
    pageSize: (query) => query.pageSize || 10
}
const ASC = 'asc'
const DESC = 'desc'
export const SORT_TYPES = {
    ASC,
    DESC
}
export const sortQuery = {
    sortBy: (sort) => SORT_TYPES[sort.sortBy] || SORT_TYPES.asc,
    sortField: (sort) => sort.sortField
}
export const initSort = {
    bankBalance: JSON.stringify({ sortOrder: DESC, sortKey: 'userId' }),
    transaction: JSON.stringify({ sortOrder: DESC, sortKey: 'userId' }),
    financial: JSON.stringify({ sortOrder: DESC, sortKey: 'financialDate' })
}
export const filterConfig = {
    ...dateQuery,
    ...sortQuery,
    ...paginatonQuery
}
