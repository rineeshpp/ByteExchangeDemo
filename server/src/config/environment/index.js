import development from './development'
import production from './production'
import staging from './staging'
import preprod from './preprod'

let CONFIG_ENV = {
    development,
    production,
    staging,
    preprod
}

const ENV = process.env.NODE_ENV && CONFIG_ENV[process.env.NODE_ENV] ? process.env.NODE_ENV : 'development'

if (process.env.NODE_ENV !== ENV) {
    console.warn(
        `No config available for '${
            process.env.NODE_ENV
        }' environment. Using '${ENV}' environment instead.`
    )
}
CONFIG_ENV = CONFIG_ENV[ENV]
export { ENV, CONFIG_ENV }
