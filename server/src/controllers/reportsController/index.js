
import express from 'express'
import gstResports from './gstController'
import transactionReports from './transactionController'
import trialBalanceReports from './trialBalanceController'

const router = express.Router()
router.get('/reports/gst-payable', gstResports)
router.get('/reports/trial-balance', trialBalanceReports)
router.get('/reports/transaction', transactionReports)

module.exports = router
