import { transactionReport } from './api'
import errorHandler from '../../services/errorHandler'

const transactionController = (req, res) => {
    return transactionReport(req, res.locals)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
}
export default transactionController
