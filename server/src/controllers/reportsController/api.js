import apiService from '../../services/apiService'
import { CONFIG_ENV } from '../../config/environment'
import { convertToCsv, queryMap, formatValue, timestampToDate } from '../../utils'
import { reportQuery, FORMAT_TYPES } from '../../constants'
import { gstKeyMapper, transactionKeyMapper, trialBalanceKeyMapper } from '../../constants/reportMapper'
const {
    API: { byteExUrl }
} = CONFIG_ENV

const gstReportUrl = `${byteExUrl}/gst-report`
const transactionReportUrl = `${byteExUrl}/transaction-tab`
const trialBalanceReportUrl = `${byteExUrl}/financial/reports`

const formatReportsData = ({ data, mapper }) => {
    const results = []
    if (!Array.isArray(data)) data = [data]
    data.map((obj) => {
        let mappedObj = {}
        mapper.forEach((mapObj) => {
            let key = mapObj.id
            if (mapObj.isNestedKey) {
                const keys = mapObj.nestedKey.split('.')
                let val = ''
                let title = String(mapObj.title)
                for (let i in keys) {
                    const k = keys[i]
                    if (obj.hasOwnProperty(k) && obj[k]) {
                        if (Number(i) === keys.length - 1) {
                            val = val.concat(obj[k])
                            break
                        }
                        val = val.concat(obj[k], ', ')
                    } else {
                        break
                    }
                }
                mappedObj[title] = val
            } else if (obj.hasOwnProperty(key)) {
                const title = String(mapObj.title)

                let value = obj[key]
                if (mapObj.type === FORMAT_TYPES.INR) {
                    value = value ? formatValue({ value, isINR: true }) : 0
                }
                if (mapObj.type === FORMAT_TYPES.DATE) {
                    value = value ? timestampToDate(value) : 'NA'
                }
                if (mapObj.type === FORMAT_TYPES.NUMBER) {
                    value = value ? Number(value) : 'NA'
                }
                mappedObj[title] = value
            }
        })
        results.push(mappedObj)
    })
    return results
}
const processCSVData = (res, resolve, reject) => {
    const { data, meta } = res
    return convertToCsv(data).then(
        csvData => resolve({ meta, data: csvData }),
        err => reject(err)
    )
}

const getGstReports = (req, reqLocals) => {
    const { byteScope: headers } = reqLocals
    let params = {
        ...req.query,
        ...queryMap(req.query, reportQuery)
    }
    return new Promise((resolve, reject) => {
        apiService
            .get({
                url: gstReportUrl,
                headers,
                reqLocals,
                params
            })
            .then(res => {
                const { data: { items: results }, meta } = res
                const formattedData = formatReportsData({ data: results || [], mapper: gstKeyMapper })
                return processCSVData({ data: formattedData, meta }, resolve, reject)
            }).catch(err => {
                reject(err)
            })
    })
}
const transactionReport = (req, reqLocals) => {
    const { byteScope: headers } = reqLocals
    let params = {
        ...req.query,
        ...queryMap(req.query, reportQuery)
    }
    return new Promise((resolve, reject) => {
        apiService
            .get({
                url: transactionReportUrl,
                headers,
                reqLocals,
                params
            })
            .then(res => {
                const { data: { orders: results }, meta } = res
                const formattedData = formatReportsData({ data: results || {}, mapper: transactionKeyMapper })
                return processCSVData({ data: formattedData, meta }, resolve, reject)
            }).catch(err => {
                reject(err)
            })
    })
}
const trialBalanceReport = (req, reqLocals) => {
    const { byteScope: headers } = reqLocals
    let params = {
        ...req.query,
        ...queryMap(req.query, reportQuery)
    }
    return new Promise((resolve, reject) => {
        apiService
            .get({
                url: trialBalanceReportUrl,
                headers,
                reqLocals,
                params
            })
            .then(res => {
                const { data: results, meta } = res
                const formattedData = formatReportsData({ data: results || [], mapper: trialBalanceKeyMapper })
                return processCSVData({ data: formattedData, meta }, resolve, reject)
            }).catch(err => {
                reject(err)
            })
    })
}

module.exports = {
    getGstReports,
    transactionReport,
    trialBalanceReport
}
