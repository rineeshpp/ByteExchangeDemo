import { trialBalanceReport } from './api'
import errorHandler from '../../services/errorHandler'

const trialBalanceController = (req, res) => {
    return trialBalanceReport(req, res.locals)
        .then(response => res.send(response))
        .catch(error => res.status(error.status || 500).send(errorHandler(error)))
}
export default trialBalanceController
