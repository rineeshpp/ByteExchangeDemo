module.exports = {
    env: {
        commonjs: true,
        node: true,
        mocha: true
    },
    extends: 'standard',
    plugins: ['mocha'],
    rules: {
        indent: ['error', 4]
    },
    'parser': 'babel-eslint'
}
