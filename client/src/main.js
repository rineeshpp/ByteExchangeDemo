import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { getCookie, iconizeInr } from "./utils";
Vue.config.productionTip = false;
Vue.filter("iconizeInr", iconizeInr);
router.beforeEach((to, from, next) => {
  const {
    userDetails: { userInfo }
  } = store.state;
  console.log(userInfo)
  if (!getCookie("app_t") && to.meta.requireAuth) {
    // next({ name: "login" });
    next();
  } else if (to.name !== "login" && !userInfo?.token && to.meta.requireAuth) {
    store.dispatch("userDetails/reloadUserSession");
    next();
  } else {
    next();
  }
});

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
