import omitBy from "lodash/omitBy";
import omit from "lodash/omit";
import isNull from "lodash/isNull";
import isUndefined from "lodash/isUndefined";
import { filterKeys } from "@/constants";
const {
  FROM_DATE,
  TO_DATE,
  TRANSACTION_STATUSES,
  PFM_CODES,
  PAYMENT_STATUS_1,
  PAYMENT_STATUS_2
} = filterKeys;
const Getters = {
  getFilterParams: state => {
    return key => {
      return state.filterParams[key];
    };
  },
  getFilterParamsWithValue: state => {
    return omitBy(state.filterParams, o => {
      return isNull(o) || isUndefined(o) || o === "" || o === "all";
    });
  },
  getSearchParamsWithValue: state => {
    const skipKeys = [
      FROM_DATE,
      TO_DATE,
      TRANSACTION_STATUSES,
      PFM_CODES,
      PAYMENT_STATUS_1,
      PAYMENT_STATUS_2
    ];
    let searchParams = omit(state.filterParams, skipKeys);
    return omitBy(searchParams, o => {
      return isNull(o) || isUndefined(o) || o === "" || o === "all";
    });
  }
};
export default { ...Getters };
