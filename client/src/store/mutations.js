import { initFilterParams } from "@/constants";
const mutations = {
  SET_FILTER_VALUE(state, payload) {
    const { filterParams } = state;
    state.filterParams = { ...filterParams, ...payload };
  },
  RESET_FILTER_VALUE(state) {
    let { userId, pran, masterTransactionId } = state.filterParams;
    state.filterParams = {
      ...initFilterParams,
      userId,
      pran,
      masterTransactionId
    };
  },
  SET_API_INFO(state, payload) {
    const { apiInfo } = state;
    if (payload) {
      state.apiInfo = { ...apiInfo, ...payload };
    } else {
      state.apiInfo = {
        message: "",
        status: null
      };
    }
  },
  ABORT_API(state, payload) {
    state.abortApi = payload;
  }
};
export default { ...mutations };
