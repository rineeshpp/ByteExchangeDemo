const Mutations = {
  SET_LOADER(state, payload) {
    state.isLoading = payload;
  },
  SET_REPORTS_DATA(state, payload) {
    state.reportsData = payload;
  }
};
export default { ...Mutations };
