import apiService from "@/services/apiService";
import { apiURLConfig } from "@/constants";
import moment from "moment";
const {
  gstReportUrl,
  trialBalanceReportUrl,
  transactionReportUrl
} = apiURLConfig;
const Actions = {
  getGstReportData: ({ commit, dispatch, rootGetters }, payload) => {
    let { getFilterParamsWithValue } = rootGetters;
    let { fromDate, toDate } = getFilterParamsWithValue;
    fromDate = fromDate ? moment(fromDate).format("YYYY-MM-DD") : fromDate;
    toDate = toDate ? moment(toDate).format("YYYY-MM-DD") : toDate;
    const { activeTab } = payload;
    let url;
    url = activeTab === "gst_payable_report" ? gstReportUrl : url;
    url = activeTab === "trial_balance_report" ? trialBalanceReportUrl : url;
    url = activeTab === "transaction_report" ? transactionReportUrl : url;
    dispatch("setApiInfo", null, { root: true });
    commit("SET_LOADER", true);
    apiService
      .get({
        url,
        params: {
          ...getFilterParamsWithValue,
          fromDate,
          toDate,
          pageSize: null
        }
      })
      .then(res => {
        commit("SET_LOADER", false);
        if (res?.data?.data?.csv) {
          commit("SET_REPORTS_DATA", res?.data?.data);
        } else {
          dispatch(
            "setApiInfo",
            { status: "success", message: "No records found." },
            { root: true }
          );
        }
      })
      .catch(err => {
        commit("SET_LOADER", false);
        commit("SET_REPORTS_DATA", null);
        if (err?.message !== "CANCELLED") {
          const message = err?.meta?.message || "Something went wrong :(";
          dispatch("setApiInfo", { status: "error", message }, { root: true });
        }
      });
  }
};

export default { ...Actions };
