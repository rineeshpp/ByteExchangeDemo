const Mutations = {
  SET_LOADER(state, payload) {
    state.isLoading = payload;
  },
  SET_TRIALBALANCE_DATA(state, payload) {
    state.trialBalanceData = payload;
  },
  SET_SORT_VALUE(state, payload) {
    state.sortItem = payload;
  }
};
export default { ...Mutations };
