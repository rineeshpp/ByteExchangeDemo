import apiService from "@/services/apiService";
import { apiURLConfig } from "@/constants";
import moment from "moment";
const { trialBalanceUrl } = apiURLConfig;
const Actions = {
  getTrialBalanceData: ({ state, commit, dispatch, rootGetters }) => {
    let { getFilterParamsWithValue } = rootGetters;
    let { fromDate, toDate } = getFilterParamsWithValue;
    let { sortItem: sort } = state;
    fromDate = fromDate ? moment(fromDate).format("YYYY-MM-DD") : fromDate;
    toDate = toDate ? moment(toDate).format("YYYY-MM-DD") : toDate;
    dispatch("setApiInfo", null, { root: true });
    commit("SET_LOADER", true);
    apiService
      .get({
        url: trialBalanceUrl,
        params: {
          ...getFilterParamsWithValue,
          fromDate,
          toDate,
          sort
        }
      })
      .then(res => {
        commit("SET_LOADER", false);
        if (res?.data?.data) {
          commit("SET_TRIALBALANCE_DATA", res?.data?.data);
        } else {
          dispatch(
            "setApiInfo",
            { status: "success", message: "No records found." },
            { root: true }
          );
        }
      })
      .catch(err => {
        commit("SET_LOADER", false);
        commit("SET_TRIALBALANCE_DATA", null);
        if (err?.message !== "CANCELLED") {
          const message = err?.meta?.message || "Something went wrong :(";
          dispatch("setApiInfo", { status: "error", message }, { root: true });
        }
      });
  }
};

export default { ...Actions };
