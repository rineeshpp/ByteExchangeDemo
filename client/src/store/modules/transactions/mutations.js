const Mutations = {
  SET_LOADER(state, payload) {
    state.isLoading = payload;
  },
  SET_TRANSACTIONS_DATA(state, payload) {
    state.transactionsData = payload;
  },
  SET_SORT_VALUE(state, payload) {
    state.sortItem = payload;
  }
};
export default { ...Mutations };
