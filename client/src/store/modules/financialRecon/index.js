import mutations from "./mutations";
import actions from "./actions";

const state = {
  isLoading: false,
  finReconData: null,
  finHeadTypes: null,
  sortItem: null,
  entryPayload: {
    financialDate: null,
    debitHead: "",
    debitAmount: null,
    creditHead: "",
    creditAmount: null,
    remarks: null
  }
};
const getters = {
  getCardData: ({ finReconData }) => {
    const { totalCount, totalDebitAmount, totalCreditAmount } =
      finReconData || {};
    return [
      {
        value: totalCount || "0",
        label: "Transaction Count"
      },
      { value: totalDebitAmount || "0.0", label: "Debit Amount", isInr: true },
      { value: totalCreditAmount || "0.0", label: "Credit Amount", isInr: true }
    ];
  },
  getTableData: ({ finReconData }) => {
    return finReconData?.tableData;
  },
  getTotalPages: ({ finReconData }) => {
    return finReconData?.totalPages;
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
