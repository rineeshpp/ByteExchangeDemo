const Mutations = {
  SET_LOADER(state, payload) {
    state.isLoading = payload;
  },
  SET_FINRECON_DATA(state, payload) {
    state.finReconData = payload;
  },
  SET_FIN_HEADTYPES(state, payload) {
    state.finHeadTypes = payload;
  },
  SET_ENTRY_PAYLOAD(state, payload) {
    let { entryPayload } = state;
    state.entryPayload = { ...entryPayload, ...payload };
  },
  RESET_ENTRY_PAYLOAD(state) {
    state.entryPayload = {
      financialDate: null,
      debitHead: "",
      debitAmount: null,
      creditHead: "",
      creditAmount: null,
      remarks: null
    };
  },
  SET_SORT_VALUE(state, payload) {
    state.sortItem = payload;
  }
};
export default { ...Mutations };
