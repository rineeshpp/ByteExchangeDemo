import apiService from "@/services/apiService";
import { apiURLConfig, APP_TOKEN } from "@/constants";
// import router from "@/router";
import { removeCookie } from "@/utils";

let { sessionReload, logoutUrl } = apiURLConfig;
let value = {
  token:
    "eyJhbGciOiJSUzUxMiJ9.eyJpc3MiOiJQQVlUTU1PTkVZIiwic3ViIjoicGF5dG1tb25leV9sb2dpbl9tb2R1bGUiLCJpZCI6InJpbmVlc2gucEBwYXl0bW1vbmV5LmNvbSIsIm5hbWUiOiJyaW5lZXNoLnBAcGF5dG1tb25leS5jb20iLCJyYW5kcGVybSI6eyJwZXJtaXNzaW9ucyI6WyJOUFNfVFJBTlNBQ1RJT05TIiwiUkVQT1JUUyIsIkJBTktfQkFMQU5DRSIsIlJFVkVOVUVfR0VORVJBVEVEIiwiQVVESVRJTkdfRU5UUklFUyJdLCJyb2xlcyI6WyJCWVRFU0NPUEVfQURNSU4iXX0sIm9yZyI6eyJhbHRfb3JnX2lkIjoiLTEiLCJvcmdfdHlwZSI6IklOVEVSTkFMIiwib3JnX25hbWUiOiJQYXl0bW1vbmV5IG9yZ2FuaXNhdGlvbiIsIm9yZ19jb2RlIjoiUEFZVE1NT05FWSJ9LCJzeXNUeXBlIjoiQllURVNDT1BFIiwiYWdlbnRJZCI6IjExMDA2MyIsImV4cCI6MTU4Nzc3OTk0M30.O8llKknrY6FuME9WDPa9c815x5H-NMhSgSkUVpJuYv3vBzchJbIjj_VRr9WZut7B9Q3-42r5FAkFafilEUc4W8zC0YUiCFqi84Dr2SAdj3H8E1QSp1wt3LSqf_bBFfsEiftXyyeS34Zn_t2omALn1r5LuA4eBBHbbNoDjS5YZFR5RkmdoG6UtkmxPzLe4CUQstych5FlvbZT99d_xLyMEaQfnrD2fUG7dWQRil0sjK8lIdyXw5852MIwl_HS1yusnoTBm7o7kYrfyTdl-bV1wmyi55anPhRDzuVeb1V5SdTOtFiFggMr2YtD09oQiHHwWaWPU2N91p6cEbF5r3Rrpw",
  user_id: "rineesh.p@gmail.com",
  agent_id: "110063",
  is_first_time: false,
  org_info: {
    name: "organisation",
    id: "MONEY",
    image_url: "",
    org_type: "INTERNAL",
    alt_org_id: "-1"
  },
  permissions: [
    "NPS_TRANSACTIONS",
    "REPORTS",
    "BANK_BALANCE",
    "REVENUE_GENERATED",
    "AUDITING_ENTRIES"
  ],
  expires: 1587779943000
};
const Actions = {
  reloadUserSession: ({ commit }) => {
    commit("SET_LOADER", true);
    apiService
      .get({ url: sessionReload })
      .then(res => {
        commit("SET_LOADER", false);

        if (res?.data?.status === 401) {
          throw res;
        } else {
          commit("SET_USER_DETAILS", value);
        }
      })
      .catch(() => {
        commit("SET_LOADER", false);
        commit("SET_USER_DETAILS", null);
        removeCookie(APP_TOKEN);
        // router.replace("/login");
      });
  },
  logout: ({ commit }, userId) => {
    commit("SET_LOADER", true);
    apiService
      .post({ url: logoutUrl, data: { userId } })
      .then(() => {
        commit("SET_LOADER", false);
        commit("SET_USER_DETAILS", null);
        // router.replace("/login");
      })
      .catch(() => {
        commit("SET_LOADER", false);
        commit("SET_USER_DETAILS", null);
        removeCookie(APP_TOKEN);
        // router.replace("/login");
      });
  }
};
export default { ...Actions };
