const Mutations = {
  SET_LOADER(state, payload) {
    state.isLoading = payload;
  },
  SET_USER_DETAILS(state, payload) {
    state.userInfo = payload;
  }
};
export default { ...Mutations };
