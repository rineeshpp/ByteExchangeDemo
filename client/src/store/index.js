import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import { initFilterParams } from "@/constants";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";
import userDetails from "./modules/userdetails";
import trialBalance from "./modules/trialBalance";
import reports from "./modules/reports";
import financialRecon from "./modules/financialRecon";
import transactions from "./modules/transactions";

export default new Vuex.Store({
  namespaced: true,
  state: {
    filterParams: {
      ...initFilterParams
    },
    apiInfo: {
      message: "",
      status: null // success||error
    },
    abortApi: null
  },
  mutations,
  actions,
  getters,
  modules: { userDetails, reports, trialBalance, financialRecon, transactions }
});
