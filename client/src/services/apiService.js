import axios from "axios";
import store from "../store";
const CancelToken = axios.CancelToken;
const clientRequest = options =>
  new Promise((resolve, reject) => {
    const source = new CancelToken.source();
    const customHeaders = setHeaders();
    options.headers = { ...options.headers, ...customHeaders };
    options.cancelToken = source.token;
    store.commit("ABORT_API", source);

    var request = axios.create();
    request(options, {
      cancelToken: source.token
    })
      .then(res => {
        store.commit("ABORT_API", null);
        resolve(res);
      })
      .catch(errorResp => {
        store.commit("ABORT_API", null);
        if (
          errorResp &&
          errorResp.response &&
          errorResp.response.status === 401 &&
          location.pathname !== "/login"
        ) {
          // location.reload();
        }
        reject(errorResp);
      });
  });

const setHeaders = () => {};

export default {
  get: options => clientRequest(options),
  post: options => {
    const requestOptions = {
      method: "POST",
      ...options
    };
    return clientRequest(requestOptions);
  },

  put: options => {
    const requestOptions = {
      method: "PUT",
      ...options
    };
    return clientRequest(requestOptions);
  }
};
