export const cardMockData = [
  {
    value: "10,00,00",
    label: "Investment Amount payable To PFRDA",
    subItem: [
      { value: "₹5,00,00,000", label: "Accural" },
      { value: "10,00,00", label: "Payable" }
    ]
  },
  { value: "1,00,00", label: "PG Receivable" },
  {
    value: "1,00,00",
    label: "Bank Balance",
    subItem: [
      { value: "₹5,00,00,000", label: "Collection" },
      { value: "10,00,00", label: "Revenue" }
    ]
  },
  {
    value: "1,00,00",
    label: "Revenue Balance",
    subItem: [
      { value: "₹5,00,00,000", label: "A/c Opening Fee" },
      { value: "10,00,00", label: "Payment Fee" }
    ]
  },
  { value: "1,00,00", label: "GST Payable" }
];
