export const mockTableData = {
  rowHeader: [
    {
      id: "test1",
      title: "First "
    },
    {
      id: "test2",
      title: "Second "
    },
    {
      id: "test3",
      title: "Therd "
    },
    {
      id: "test4",
      title: "four "
    },
    {
      id: "test5",
      title: "five "
    }
  ],
  rowData: [
    {
      test1: "1",
      test2: "2",
      test3: "3",
      test4: "3",
      test5: "3"
    },
    {
      test1: "01",
      test2: "02",
      test3: "03",
      test4: "03",
      test5: "03"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    },
    {
      test1: "10",
      test2: "20",
      test3: "30",
      test4: "30",
      test5: "30"
    }
  ]
};
