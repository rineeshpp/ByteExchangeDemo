import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/views/Login.vue";
import Layout from "@/views/Panel/Layout";
import Reports from "@/views/Panel/Reports";
import Transactions from "@/views/Panel/Transactions";
import FinancialRecon from "@/views/Panel/FinancialRecon";
import BankBalance from "@/views/Panel/BankBalance";
Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/",
    name: "Layout",
    component: Layout,
    meta: {
      title: "",
      requireAuth: true,
      permissions: ["NPS_TRANSACTIONS"]
    },
    redirect: "/transactions",
    children: [
      {
        path: "/transactions",
        name: "Transactions",
        component: Transactions,
        redirect: "/transactions/investment",
        meta: {
          title: "Transactions",
          permissions: ["NPS_TRANSACTIONS"],
          requireAuth: true
        },
        children: [
          {
            path: "investment/:tierId?",
            name: "Investment",
            meta: {
              title: "Investment",
              permissions: ["NPS_TRANSACTIONS"],
              requireAuth: true,
              tabType: "purchase"
            }
          },
          {
            path: "redemption/",
            name: "Redemption",
            redirect: "redemption/tier2",
            meta: {
              title: "Redemption",
              requireAuth: true,
              tabType: "redemption",
              permissions: ["NPS_TRANSACTIONS"]
            },
            children: [
              {
                path: ":tierId",
                name: "Redemption-tier2",
                meta: {
                  title: "Redemption",
                  requireAuth: true,
                  permissions: ["NPS_TRANSACTIONS"],
                  tabType: "redemption"
                }
              }
            ]
          }
        ]
      },
      {
        path: "/revenue",
        name: "Revenue",
        component: Transactions,
        redirect: "/revenue/account-opening-fee",
        meta: {
          title: "Revenue",
          permissions: ["REVENUE_GENERATED"],
          requireAuth: true,
          tabType: "revenue"
        },
        children: [
          {
            path: "account-opening-fee",
            name: "OpeningFee",
            meta: {
              title: "Account Opening Fee",
              requireAuth: true,
              permissions: ["REVENUE_GENERATED"],
              tabType: "revenue",
              revenueType: "registrationFees"
            }
          },
          {
            path: "payment-charges",
            name: "PaymentCharges",
            meta: {
              title: "Payment Charges",
              requireAuth: true,
              permissions: ["REVENUE_GENERATED"],
              tabType: "revenue",
              revenueType: "paymentFees"
            }
          }
        ]
      },
      {
        path: "/bank-balance",
        name: "Bank_balance",
        component: BankBalance,
        meta: {
          title: "Bank balance",
          permissions: ["BANK_BALANCE"],
          requireAuth: true
        }
      },
      {
        path: "/financial-recon",
        name: "Financial_recon",
        component: FinancialRecon,
        meta: {
          title: "Financial recon",
          permissions: ["AUDITING_ENTRIES"],
          requireAuth: true
        }
      },
      {
        path: "/reports",
        name: "Reports",
        component: Reports,
        meta: {
          title: "Reports",
          permissions: ["REPORTS"],
          requireAuth: true
        }
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
